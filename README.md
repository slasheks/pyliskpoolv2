# pyliskpoolv2
Pool 

```
cd ~
git clone https://github.com/slasheks/pyliskpoolv2.git
cd pyliskpoolv2
```

```
0 * * * * ~/pyliskpoolv2/pyliskcron.bash  >> /tmp/pyliskpool.log 2>&1
```

```
python3 pyliskpool --dry-run --payout
```

```
python3 pyliskpool --dry-run --payout --online --second-pass
```

```
python3 pyliskpool --dry-run --online --payout --second-pass --config ~/pyliskpoolv2/config.yaml -e ~/pyliskpoolv2/exception_list.yaml --db ~/pyliskpoolv2/pyliskpool.sqlite
```
