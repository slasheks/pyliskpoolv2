python3 ~/pyliskpoolv2/pyliskpool\
  --config ~/pyliskpoolv2/config.yaml\
  --db ~/pyliskpoolv2/pyliskpool.sqlite\
  --exception-list ~/pyliskpoolv2/exception_list.yaml
