#!/usr/bin/env python3

import sys
import json
import logging
import sqlite3
import argparse

def main(args):
    """
    """
    statement = None

    if args.payout_summary:
        statement = 'SELECT * FROM payout_summary'
    elif args.payout_summary_clear:
        statement = 'DROP TABLE payout_summary'
    elif args.payout_aggregate:
        statement = 'SELECT * FROM payout_aggregate'
    elif args.aggr_full:
        statement = 'SELECT * FROM payout_aggregate WHERE rewards > {}'\
            .format(args.rwd)
    elif args.payout_aggregate_clear:
        statement = 'DROP TABLE payout_aggregate'
    elif args.payout_details:
        statement = 'SELECT * FROM payout_details'
    elif args.transactions:
        statement = 'SELECT * FROM transactions'
    elif args.transactions_clear:
        statement = 'DROP TABLE transactions'
    else:
        PARSER.print_help()
        sys.exit()

    resp = db_query(args.db_file, statement)

    pretty_printer(args.output, args.sum, resp)

def db_query(db_file, statement):
    """
    """
    conn = sqlite3.connect(db_file)
    cursor = conn.cursor()
    
    try:
        cursor.execute(statement)
    except Exception as err:
        return str(err)

    if cursor.description is None:
        return None
    
    column_names = [desc[0] for desc in cursor.description]
    
    results = []

    for row in cursor:
        new_row = [x.tobytes().decode() if isinstance(x, memoryview)
                   else x for x in row]
        results.append(dict(zip(column_names, new_row)))
    
    return results

def pretty_printer(output, summ, results):
    """
    """
    if output == 'json':
        if summ:
            try:
                print(sum([x['forged_to_give'] for x in results]))
            except:
                print(sum([x['rewards'] for x in results]))
        else:
            print(json.dumps({'data': results}, indent=3))
    elif output == 'text':
        for item in results:
            print(item)

def usage():
    """
    """
    return '''

    ./db_helper.py --aggr-full --rwd 2

    ./db_helper.py --transactions

    ./db_helper.py --payout-summary

    ./db_helper.py --payout-summary --sum

    ./db_helper.py --payout-aggregate

    ./db_helper.py --payout-aggregate --sum

    ./db_helper.py --payout-details
    '''

if __name__ == '__main__':

    PARSER = argparse.ArgumentParser(description='LISK Pool DB Helper',
                                     usage=usage())

    PARSER.add_argument('--output', dest='output', action='store',
                        choices=('json','text','csv'),
                        default='json', help='')

    PARSER.add_argument('--sum', dest='sum',
                        action='store_true', help='')

    PARSER.add_argument('--rwd', dest='rwd', type=int,
                        action='store', help='')

    PARSER.add_argument('--transactions', dest='transactions',
                        action='store_true', help='')

    PARSER.add_argument('--transactions-clear', dest='transactions_clear',
                        action='store_true', help='')

    PARSER.add_argument('--payout-summary', dest='payout_summary',
                        action='store_true', help='')

    PARSER.add_argument('--payout-details', dest='payout_details',
                        action='store_true', help='')

    PARSER.add_argument('--payout-summary-clear', dest='payout_summary_clear',
                        action='store_true', help='')

    PARSER.add_argument('--payout-aggregate', dest='payout_aggregate',
                        action='store_true', help='')

    PARSER.add_argument('--aggr-full', dest='aggr_full',
                        action='store_true', help='')

    PARSER.add_argument('--payout-aggregate-clear', dest='payout_aggregate_clear',
                        action='store_true', help='')

    PARSER.add_argument('--db-file', dest='db_file',
                        default="pyliskpool.sqlite", action='store',
                        help='Config file. Default is ./config.yaml')

    ARGS = PARSER.parse_args()

    logging.getLogger("requests").setLevel(logging.WARNING)
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger()

    main(ARGS)
